@article{Sommariva2008,
    author = { A. Sommariva and M. Vianello and R. Zanovello },
    doi = { https://doi.org/10.1007/s11075-008-9203-x },
    issue = { 1-4 },
    journal = { Numerical Algorithms },
    language = { en },
    month = { 12 },
    pages = { 409--427 },
    publisher = { Springer Science and Business Media {LLC} },
    title = { Nontensorial {Clenshaw–Curtis} Cubature },
    volume = { 49 },
    year = { 2008 }
}
