# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import pathlib

import pytest

import papis_extras.config  # noqa: F401
from papis.testing import TemporaryConfiguration

datadir = pathlib.Path(__file__).parent / "data"


@pytest.mark.config_setup(
    settings={
        "multiple-authors-format": "{au[given]} {au[family]}",
        "extras-exporter-capitalize-words": datadir / "wordlist.txt",
    }
)
@pytest.mark.parametrize("basename", ["test_exporter_plain_0", "test_exporter_plain_1"])
def test_exporter_plain(tmp_config: TemporaryConfiguration, basename: str) -> None:
    from testlib import check_from_file, load_document_from_file

    doc = load_document_from_file(basename)

    from papis_extras.exporters.plain import export_doc_plain

    exported = export_doc_plain(doc)

    assert check_from_file(exported, basename)


@pytest.mark.config_setup(
    settings={
        "multiple-authors-format": "{au[given]} {au[family]}",
        "extras-exporter-capitalize-words": datadir / "wordlist.txt",
    }
)
@pytest.mark.parametrize("basename", ["test_exporter_biber_0", "test_exporter_biber_1"])
def test_exporter_biber(tmp_config: TemporaryConfiguration, basename: str) -> None:
    from testlib import check_from_file, load_document_from_file

    doc = load_document_from_file(basename)

    from papis_extras.exporters.biber import export_doc_biblatex

    exported = export_doc_biblatex(doc)

    assert check_from_file(exported, basename)


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
