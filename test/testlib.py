# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import pathlib
from abc import ABC, abstractmethod
from typing import Any

import papis.config
import papis.database
import papis.document
import papis.yaml

logging.basicConfig()
logger = logging.getLogger("testlib")
logger.setLevel(logging.INFO)


# {{{ load document


def get_datadir(basedir: str | None) -> pathlib.Path:
    if basedir is None:
        datadir = pathlib.Path(__file__).parent
    else:
        datadir = pathlib.Path(basedir)

    return datadir / "data"


def load_document_from_file(
    basename: str, *, basedir: str | None = None
) -> papis.document.Document:
    """Loads a document from a YAML file."""
    dirname = get_datadir(basedir)

    filename = dirname / f"{basename}.yml"
    data = papis.yaml.yaml_to_data(str(filename))
    return papis.document.from_data(data)


def check_from_file(result: str, basename: str, *, basedir: str | None = None) -> bool:
    """Checks result against stored expected values."""
    dirname = get_datadir(basedir)

    filename = dirname / f"{basename}_expected.txt"
    if filename.exists():
        with open(filename, encoding="utf-8") as fd:
            expected = fd.read().strip()
    else:
        with open(filename, "w", encoding="utf-8") as fd:
            fd.write(result)

        expected = result

    is_same = expected == result
    if not is_same:
        logger.info("\nEXPECTED:\n%s\nGOT:\n%s", expected, result)

    return is_same


# }}}


# {{{ load dict


class DictFile(ABC):
    @abstractmethod
    def dump(self, data: dict[str, Any]) -> None:
        pass

    @abstractmethod
    def load(self) -> dict[str, Any]:
        pass


class JSONFile(DictFile):
    def __init__(self, filename: str) -> None:
        self.filename = filename

    def dump(self, data: dict[str, Any]) -> None:
        import json

        with open(self.filename, "w", encoding="utf-8") as f:
            json.dump(data, f, indent=4, sort_keys=True)

    def load(self) -> dict[str, Any]:
        import json

        with open(self.filename, encoding="utf-8") as f:
            result = json.load(f)

        if not isinstance(result, dict):
            raise TypeError(f"invalid data loaded from '{self.filename}'")

        return result


class YAMLFile(DictFile):
    def __init__(self, filename: str) -> None:
        self.filename = filename

    def dump(self, data: dict[str, Any]) -> None:
        import yaml

        with open(self.filename, "w", encoding="utf-8") as f:
            yaml.dump(data, f)

    def load(self) -> dict[str, Any]:
        import yaml

        with open(self.filename, encoding="utf-8") as f:
            result = yaml.safe_load(f)

        if not isinstance(result, dict):
            raise TypeError(f"invalid data loaded from '{self.filename}'")

        return result


def load_dict(
    filename: str,
    *,
    data: dict[str, Any] | None = None,
    basedir: str | None = None,
) -> dict[str, Any]:
    """Loads JSON or YAML data from a file."""
    dirname = get_datadir(basedir)

    infile = dirname / filename
    if infile.suffix == ".json":
        f: DictFile = JSONFile(filename)
    elif infile.suffix == ".yaml":
        f = YAMLFile(filename)
    else:
        raise ValueError(f"unknown file extension: '{infile.suffix}'")

    if not infile and data is not None:
        logger.info("dumping data to file '%s'", infile)
        f.dump(data)
        return data

    return f.load()


# }}}
