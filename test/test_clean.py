# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from typing import Any

import pytest

import papis.document
import papis_extras.config  # noqa: F401
from papis.testing import TemporaryConfiguration


def _check_author(
    author_list: dict[str, Any], expected_author: str | None = None
) -> None:
    from papis_extras.cleaners import author_list_cleaner

    doc = papis.document.from_data({
        "author_list": [author_list],
    })

    doc["author_list"] = author_list_cleaner(doc)

    from papis_extras.utils import author_list_to_author

    test_author = author_list_to_author(doc, abbrev_given=False)

    if expected_author is not None:
        assert test_author == expected_author, (
            f"got `{test_author}`, expected `{expected_author}`"
        )


def test_author_cleaner(tmp_config: TemporaryConfiguration) -> None:
    papis.config.set("multiple-authors-format", "{au[given]} {au[family]}")
    papis.config.set("multiple-authors-separator", "and")

    # check: 'de la' capitalization
    _check_author(
        {"given": "kate", "family": "de la Katington"}, "Kate de la Katington"
    )
    # check: removing trailing spaces
    _check_author({"given": "Kate  ", "family": "  Katington"}, "Kate Katington")
    # check: adding spaces after fullstop
    _check_author({"given": "K.K.", "family": "Katington"}, "K. K. Katington")
    # check: handling apostrophe ' in name
    _check_author({"given": "M'hamed", "family": "Souli"}, "M'hamed Souli")
    # check: not adding spaces if full stop is followed by "-"
    _check_author({"given": "P.-E.", "family": "Bernard"}, "P.-E. Bernard")


def test_ref_cleaner(tmp_config: TemporaryConfiguration) -> None:
    # NOTE: fix this in case the defaults change
    papis.config.set("ref-format", "{doc['doi']}")

    ref_formats = ["first-author-year", "initials-year"]

    documents = []
    # check: single author
    documents.append({
        "year": 1990,
        "author_list": [{"given": "Kate", "family": "Katington"}],
    })
    # check: multiple authors
    documents.append({
        "year": 1990,
        "author_list": [
            {"given": "Sam", "family": "Saminsky"},
            {"given": "Kate", "family": "Katington"},
        ],
    })
    # check: author with no family name
    documents.append({"year": 1990, "author_list": [{"given": "Kate"}]})
    # check: no authors
    documents.append({"year": 1990})

    expected: tuple[tuple[str | None, str | None], ...] = (
        ("Katington1990", "K1990"),
        ("Saminsky1990", "SK1990"),
        (None, None),
        (None, None),
    )

    from papis_extras.cleaners import ref_cleaner

    for iref, ref_format in enumerate(ref_formats):
        for idoc, doc in enumerate(documents):
            doc_ = papis.document.from_data(doc)
            result = ref_cleaner(doc_, ref_format)
            assert result == expected[idoc][iref], result


def test_string_cleaner(tmp_config: TemporaryConfiguration) -> None:
    strings = []
    # check: no space is added after the fullstop
    strings.append(("3.5-D Blocking Optimization", "3.5-D Blocking Optimization"))

    from papis_extras.cleaners import string_cleaner_impl

    for orig, expected in strings:
        cleaned = string_cleaner_impl(orig)
        assert cleaned == expected, cleaned


def test_tags_cleaner(tmp_config: TemporaryConfiguration) -> None:
    doc_data: tuple[dict[str, Any], ...] = (
        {},
        {"tags": "tag1 tag2"},
        {"tags": "tag1, tag2"},
        {"tags": ["tag1", "tag2"]},
    )
    expected_tags: tuple[str | None, ...] = (
        None,
        "tag1 tag2",
        "tag1 tag2",
        "tag1 tag2",
    )

    from papis_extras.cleaners import tags_cleaner

    for data, tags in zip(doc_data, expected_tags):
        doc = papis.document.from_data(data)
        new_tags = tags_cleaner(doc)

        assert new_tags == tags


def test_pages_cleaner(tmp_config: TemporaryConfiguration) -> None:
    data_pages: tuple[Any, ...] = (
        "10-111",
        "10--111",
        "10",
        "A201-A210",
        10,
        [11, 123],
    )
    expected_pages: tuple[str, ...] = (
        "10-111",
        "10-111",
        "10-10",
        "A201-A210",
        "10-10",
        "11-123",
    )

    from papis_extras.cleaners import pages_cleaner

    for data, pages in zip(data_pages, expected_pages):
        doc = papis.document.from_data({"pages": data})
        new_pages = pages_cleaner(doc)

        assert new_pages == pages


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        exec(sys.argv[1])
    else:
        pytest.main([__file__])
