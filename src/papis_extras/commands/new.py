# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT
#
# SPDX-FileCopyrightText: Papis Developers
# SPDX-License-Identifier: GPL-3.0-or-later
#
# NOTE: the get_unique_folder_name was originally written by Alejandro Gallo
# in papis and modified by other contributors, so it goes under the
# GPL-3.0-or-later license.

"""A wrapper around ``papis add`` which also calls ``papis clean``.

Command-line Interface
^^^^^^^^^^^^^^^^^^^^^^

.. click:: papis_extras.commands.new:cli
    :prog: papis new
"""

from __future__ import annotations

import os
from typing import Any

import click

import papis.cli
import papis.config
import papis.document
import papis.importer
import papis.logging
import papis_extras.config  # noqa: F401
from papis_extras.utils import FormattedString, getformattedstring  # type: ignore

logger = papis.logging.get_logger(__name__)


def get_unique_folder_name(
    files: list[str],
    data: dict[str, Any],
    folder_name_format: FormattedString | None = None,
    subfolder: str | None = None,
    max_path_length: int = 150,
) -> str:
    """
    Generates a unique folder for the document

    :param data: Data parsed from the document
    :param subfolder: folder within the library where the document should be
        stored
    :param folder_name: format for the folder name
    :returns: an absolute folder path
    """

    if folder_name_format is None:
        from papis.commands.add import get_hash_folder

        basename = get_hash_folder(data, files)
        logger.info("Got an automatic folder name.")
    else:
        doc = papis.document.Document(data=data)
        basename = papis.utils.clean_document_name(
            papis.format.format(folder_name_format, doc)
        )
        del doc

    if len(basename) > max_path_length:
        logger.warning("Shortening the name '%s' for portability.", basename)
        basename = basename[:max_path_length]

    # get library folder name
    lib_dirs = papis.config.get_lib_dirs()
    if len(lib_dirs) == 0:
        raise RuntimeError(f"Library {papis.config.get_lib_name()} has no folders.")

    dirname = os.path.expanduser(os.path.join(lib_dirs[0], subfolder or ""))

    # ensure the folder path is unique
    folder_path_orig = os.path.join(dirname, basename)
    folder_path = folder_path_orig

    from string import ascii_lowercase

    g = papis.utils.create_identifier(ascii_lowercase)
    while os.path.exists(folder_path):
        string_append = next(g)
        folder_path = f"{folder_path_orig}-{string_append}"

    logger.info("The folder name is '%s'.", basename)
    logger.debug("Folder path is '%s'", folder_path)
    if "files" in data:
        logger.debug("File(s): '%s'", "', '".join(data["files"]))

    return folder_path


@click.command("new", help="Add a document into a given library")
@click.help_option("--help", "-h")
@click.option(
    "-s",
    "--set",
    "sets",
    help="Set document information (title, authors, etc.)",
    multiple=True,
    type=(str, str),
)
@click.option(
    "--from",
    "from_importer",
    help="Add document from a specific importer",
    type=(click.Choice(papis.importer.available_importers()), str),
    nargs=2,
    default=(None, ""),
)
@click.argument("files", type=click.Path(exists=True), nargs=-1)
def cli(
    files: list[str],
    sets: list[tuple[str, str]],
    from_importer: tuple[str, str],
) -> None:
    if not files:
        logger.error("Files must be specified.")
        return

    # {{{ import

    ctx = papis.importer.Context()

    if from_importer:
        from papis.utils import collect_importer_data, get_matching_importer_by_name

        importers = get_matching_importer_by_name([from_importer], only_data=True)
        imported = collect_importer_data(importers, batch=False, only_data=True)

        ctx.data.update(imported.data)

    ctx.files = list(files)

    from itertools import groupby

    partitioned_sets: list[list[tuple[str, str]]] = [[], []]
    for k, v in groupby(sets, key=lambda k: k[0] in ctx.data):
        partitioned_sets[k] = list(v)

    upd_sets, new_sets = partitioned_sets[True], partitioned_sets[False]
    ctx.data.update(new_sets)

    if upd_sets:
        if ctx.data:
            papis.utils.update_doc_from_data_interactively(
                ctx.data, dict(upd_sets), "command-line"
            )
        else:
            ctx.data.update(upd_sets)

    if not ctx:
        logger.error("There is nothing to be added.")
        return

    # }}}

    # {{{ add document

    folder_name = get_unique_folder_name(
        ctx.files,
        ctx.data,
        folder_name_format=getformattedstring("add-folder-name"),
    )

    import logging

    from papis.commands.add import run as add

    logging.getLogger("papis.commands.add").setLevel(logging.ERROR)
    add(
        ctx.files,
        data=ctx.data,
        folder_name=folder_name,
        file_name=None,
        subfolder="",
        confirm=bool(papis.config.getboolean("add-confirm")),
        open_file=bool(papis.config.getboolean("add-open")),
        edit=bool(papis.config.getboolean("add-edit")),
        git=False,
        link=False,
    )

    # }}}

    # {{{ clean document

    from papis_extras.commands.clean import run as clean

    doc = papis.document.from_folder(folder_name)
    if not doc:
        # NOTE: probably cancelled adding the document, so no need to clean it!
        return

    clean(
        [doc],
        keys=papis.config.getlist("keys", section="extras-clean"),
        ref_format=getformattedstring("ref-format", section="extras-clean"),
        dry_run=False,
        git=False,
    )

    # }}}

    move = papis.config.getboolean("move", section="extras-new")
    if not move:
        return

    for f in files:
        try:
            os.remove(f)
        except OSError as exc:
            logger.error("Failed to remove file: '%s'.", f, exc_info=exc)
