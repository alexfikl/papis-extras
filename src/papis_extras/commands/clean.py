# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
This command can be used to sanitize / clean the information in a
document. For example, a few interesting customized choices are

* `ref`: rewrite the reference in the document to the desired format.
  The format can be specified by the ``ref-format`` command-line option or
  configuration key.
* `time-added`: adds a timestamp to the document based on the creation date
  of the oldest file in the document (only if not already present).
* `author`: assumes that the ``author_list`` is present and tries to clean
  up the names and update the ``author`` field with the current format
  present in the config (see ``multiple-authors-format`` and
  ``multiple-authors-separator``).
* `title`, `journal` and some others are titlecased using
  `titlecase <https://pypi.org/project/titlecase/>`_ and a few special rules
  for acronyms.
* other string keys are also cleaned up a bit too, e.g. removing new lines or
  repeated spaces.
* `files`: updates the file names if the other fields have changed.
* *folder*: updates the folder name if the other fields have changed.

Updating the file names is tweaked quite a bit with respect to ``papis``.
The main change comes in having more control over the author list. For this,
we use ``file-author-format`` and ``file-author-separator`` (which
fall back to ``multiple-authors-format`` and ``multiple-authors-separator``,
respectively) to describe the format and allow a maximum of 4 authors to
reduce the file name length.

Folder names work exactly the same, but are only changed in the user
specified a ``add-folder-name``. If two folders with the same name pop up,
we just add letters at the end until the name becomes unique.

Examples
^^^^^^^^

- To titlecase the title of a paper, simply call

.. code::

    papis clean Einstein --key title

- To set the reference to a desired format, call

.. code::

    papis clean Einstein --key ref --ref-format "first-author-year"

- To update the author and author list, call

.. code::

    papis clean Einstein --key author

- To update folder and file names, call

.. code::

    papis clean Einstein --key folder --key files

- These commands call all be called together, for example like

.. code::

    papis clean Einstein --key author --key title --key journal


Options
^^^^^^^

.. papis-config:: keys
    :section: extras-clean

    A list of keys that are cleaned by default, when no other options are
    given.

.. papis-config:: ref-format
    :section: extras-clean

    The format used when modifying the ``ref`` field in a document. This
    overwrites the default ``ref-format`` option used by papis, but falls
    back to it when the desired reference format cannot be implemented (e.g.
    authors are not provided).
    Currently, the following formats are implemented:

    * `"doi"`: uses the ``doi`` key as a reference.
    * `"isbn"`: uses the ``isbn`` or ``isbn-13`` keys as a reference.
    * `"first-author-year"`: uses something like ``Kim1985`` as the
      reference, where ``Kim`` is the family name of the first author
      (in the order given in ``author_list``).
    * `"initials-year"`: uses something like ``KM1985`` as the reference,
      where the family names of the authors are ``Kim`` and ``Moin``.
    * `"authors-year"`: uses something like ``KimMoin1985`` as the reference.
      Only the first 4 authors are used, to not make this too long.

.. papis-config:: file-author-format
    :section: extras-clean

    Overwrites the papis option ``multiple-authors-format`` when deriving the
    name of the authors while renaming files or folders.

.. papis-config:: file-author-separator
    :section: extras-clean

    Overwrites the papis option ``multiple-authors-separator`` when deriving
    the name of the authors while renaming files or folders.

Command-line Interface
^^^^^^^^^^^^^^^^^^^^^^

.. click:: papis_extras.commands.clean:cli
    :prog: papis clean
"""

from __future__ import annotations

import os
import sys
from collections.abc import Iterable, Sequence
from typing import Any

import click

import papis.api
import papis.cli
import papis.config
import papis.document
import papis.format
import papis.git
import papis.logging
import papis_extras.config  # noqa: F401
from papis_extras.utils import FormattedString, getformattedstring  # type: ignore

logger = papis.logging.get_logger(__name__)


# reference formats
_ref_formats = ["doi", "isbn", "first-author-year", "initials-year", "authors-year"]


def sort_keys(keys: Sequence[str]) -> list[str]:
    priority = {"author": 2, "title": 2, "ref": 1, "files": -1, "folder": -2}

    return sorted(keys, key=lambda k: priority.get(k, 0), reverse=True)


def copy_files(doc: papis.document.Document, files: Sequence[str]) -> None:
    import shutil

    for old_file_path, new_filename in zip(doc.get_files(), files):
        old_filename = os.path.basename(old_file_path)
        if old_filename == new_filename:
            continue

        old_dirname = os.path.dirname(old_file_path)
        new_file_path = os.path.join(old_dirname, new_filename)
        if os.path.exists(new_file_path):
            logger.warning("New file already exists: '%s'.", new_filename)
            continue

        shutil.copyfile(old_file_path, new_file_path)
        os.remove(old_file_path)

    # update entry
    doc["files"] = files


def clean_document_keys(
    doc: papis.document.Document, keys: Sequence[str], ref_format: FormattedString | str
) -> papis.document.Document | None:
    import colorama

    def changed_lines(key_old: str, key_new: str) -> list[str]:
        return [
            "{c.Fore.RED}-{name}{c.Style.RESET_ALL} {value}".format(
                c=colorama, name=key_old, value=old_data.get(key_old, "")
            ),
            "{c.Fore.GREEN}+{name}{c.Style.RESET_ALL} {value}".format(  # noqa: UP032
                c=colorama, name=key_old, value=doc[key_new]
            ),
        ]

    # parse remaining keys
    lines = [
        "{c.Fore.YELLOW}{folder}{c.Style.RESET_ALL}:".format(  # noqa: UP032
            c=colorama, folder=doc.get_main_folder()
        )
    ]
    errors = []

    import papis_extras.cleaners as cl
    from papis_extras.utils import author_list_to_author

    old_data = dict(doc)
    old_data["folder"] = doc.get_main_folder()

    for key in keys:
        value = doc.get(key, None)

        key_new = key
        value_new: Any = None

        try:
            if key == "ref":
                value_new = cl.ref_cleaner(doc, ref_format)
            elif key == "time-added":
                value_new = cl.timestamp_cleaner(doc)
            elif key == "author":
                doc["author_list"] = list(cl.author_list_cleaner(doc))
                value_new = author_list_to_author(doc, abbrev_given=False)
            elif key == "files":
                key_new = "files_"
                value_new = cl.files_cleaner(doc)
                value_new = list(value_new) if value_new is not None else value_new
            elif key == "folder":
                key_new = "folder_"
                value_new = cl.folder_cleaner(doc)
            elif key == "tags":
                value_new = cl.tags_cleaner(doc)
            elif key == "pages" and value is not None:
                value_new = cl.pages_cleaner(doc)
            elif key == "year" and isinstance(value, str):
                value_new = int(value)
            elif key == "month" and isinstance(value, str):
                value_new = cl.month_cleaner(value)
            elif key == "type":
                pass
            elif isinstance(value, str):
                value_new = cl.string_cleaner(
                    doc, key, titlecased=cl.is_title_like(key)
                )
            elif isinstance(value, (list, tuple, dict)):
                value_new = cl.iterable_cleaner(
                    doc, key, titlecased=cl.is_title_like(key)
                )
            elif isinstance(value, (int, float)):
                value_new = value
            elif value is None:
                logger.debug("Key '%s' not in document.", key)
            else:
                errors.append(
                    f"Key '{key}' of unknown type "
                    f"<{type(value).__name__}> was not cleaned."
                )
        except Exception as e:
            value_new = None
            errors.append(f"Key '{key}' exception: {type(e).__name__}: {e}.")

        if value_new is not None and value_new != value:
            doc[key_new] = value_new
            lines.extend(changed_lines(key, key_new))

            if key == "author":
                lines.extend(changed_lines("author_list", "author_list"))

    changed = len(lines) > 1
    if changed:
        logger.info("%s\n", "\n\t".join(lines))
        sys.stdout.flush()

    if len(errors) >= 1:
        logger.info("%s\n", "\n\t".join(errors))

    return doc if changed else None


def save_changes(documents: Iterable[papis.document.Document]) -> None:
    logger.info("Updating database...")

    from papis.tui.utils import progress_bar

    db = papis.database.get()
    for doc in progress_bar(documents):
        db.delete(doc)

        if "files_" in doc:
            copy_files(doc, doc.pop("files_"))
        if "folder_" in doc:
            papis.document.move(doc, doc.pop("folder_"))

        doc.save()
        db.add(doc)


def run(
    documents: Sequence[papis.document.Document],
    keys: Sequence[str],
    *,
    ref_format: FormattedString | str = "",
    dry_run: bool = False,
    git: bool = False,
) -> None:
    import multiprocessing
    from functools import partial

    if dry_run:
        git = False

    cleaner = partial(clean_document_keys, keys=sort_keys(keys), ref_format=ref_format)

    cpus = multiprocessing.cpu_count()
    with multiprocessing.Pool(cpus) as pool:
        all_documents = pool.map(cleaner, documents)

    modified_documents = [d for d in all_documents if d is not None]
    nmodified = len(modified_documents)
    if nmodified == 0:
        logger.info("All documents are clean.")
        return

    if dry_run:
        logger.info(
            "Found %d document%s with issues.", nmodified, "" if nmodified == 1 else "s"
        )
    else:
        logger.info("Cleaned %d document%s.", nmodified, "" if nmodified == 1 else "s")
        save_changes(modified_documents)

    if git:
        papis.git.add_and_commit_resource(
            str(papis.config.get_lib_dirs()[0]), ".", "Cleaned up document keys"
        )


@click.command()
@click.help_option("--help", "-h")
@papis.cli.query_argument()
@click.option(
    "--key",
    "-k",
    help="fields to clean",
    type=str,
    multiple=True,
    default=lambda: papis.config.getlist("keys", section="extras-clean"),
)
@click.option(
    "--ref-format",
    help="format to use for reference",
    type=click.Choice(_ref_formats),
    default=lambda: getformattedstring("ref-format", section="extras-clean"),
)
@click.option(
    "--dry",
    help="run without performing any modifications",
    default=False,
    is_flag=True,
)
@papis.cli.git_option(help="add and commit changes to documents")
def cli(query: str, key: list[str], ref_format: str, dry: bool, git: bool) -> None:
    """Clean document keys and update dependent entries"""
    documents = papis.database.get().query(query)

    keys = set(key)
    unsupported_keys: list[str] = []
    for k in unsupported_keys:
        if k in keys:
            logger.warning("Cleaner for '%s' not implemented.", k)
            keys.remove(k)

    if "author_list" in keys:
        keys.add("author")
        keys.remove("author_list")

    if not keys:
        logger.info("Nothing to do.")
        return

    run(documents, list(keys), ref_format=ref_format, dry_run=dry, git=git)
