# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import os
import re
from contextlib import suppress
from typing import Any, TypeVar

import papis.config
import papis.document
import papis.logging
from papis_extras.utils import FormattedString, getformattedstring  # type: ignore

logger = papis.logging.get_logger(__name__)

T = TypeVar("T")


def _get_user_words(key: str) -> frozenset[str]:
    with suppress(SyntaxError):
        return frozenset(papis.config.getlist(key, section="extras-exporter"))

    import pathlib

    filename = pathlib.Path(papis.config.getstring(key, section="extras-exporter"))
    if not filename.is_absolute():
        config = pathlib.Path(papis.config.get_config_file())
        filename = config.parent / filename

    if not filename.exists():
        raise FileNotFoundError(filename)

    with open(filename, encoding="utf-8") as fd:
        return frozenset([w for w in fd.read().split("\n") if w])


# keys to titlecase
TITLE_LIKE_KEYS = frozenset([
    "title",
    "journal",
    "publisher",
    "booktitle",
    "chapter",
    "school",
    "institution",
])

# words to always uppercase
_DEFAULT_UPPERCASE_WORDS = frozenset((
    # misc
    "2D",
    "3D",
    "LLC",
    "BV",
    "IBM",
    # journals and publishers
    "SIAM",
    "AIAA",
    "ACM",
    "IEEE",
    "JSTOR",
    "ASME",
    "OSA",
    "CRC",
    "VCH",
    "ZAMM",
    # schools and institutions
    "INRIA",
    "ONERA",
    "MIT",
    # popular numerical methods
    "VOF",
    "THINC",
    "BEM",
    "WENO",
    "PLIC",
    "DNS",
    "LES",
    "FEM",
    "CFD",
    # math
    "ODE",
    "PDE",
))

# words with given capitalization
_DEFAULT_CAPITALIZE_WORDS = frozenset((
    # scientists
    "Laplace",
    "Helmholtz",
    "Newton",
    "Gauss",
    "Einstein",
    "Maxwell",
    "Hooke",
    "Euler",
    "Pascal",
    "Euclid",
    "Riemann",
    "Fibonacci",
    "Neumann",
    "Poincaré",
    "Lagrange",
    "Legendre",
    "Galois",
    "Fourier",
    "Cauchy",
    "Bernoulli",
    "Navier",
    "Stokes",
    "Galerkin",
    "Stefan",
    "Runge",
    "Kutta",
    "Beltrami",
    "Sobolev",
    "Reynolds",
    "Prandtl",
    "Taylor",
    "Kolmolgorov",
    "Landau",
    "Mach",
    "Crank",
    "Nicolson",
    "Hilbert",
))

# string cleaning
# removes html tags
_RE_CLEAN_HTML = re.compile(r"<.*?>")
# removes leading, trailing and repeating whitespace
_RE_CLEAN_WHITESPACE = re.compile(r"\s+")
# adds dot after single letter names
_RE_CLEAN_DOTS = re.compile(r"(\b\w\b)([^\.']|$)")
# add space after punctuation
_RE_CLEAN_FULLSTOP = re.compile(r"[\.:;,]([^\d -])")
# split words
_RE_CLEAN_SPLIT = re.compile(r"\W+")
# capitalize roman numerals
_RE_CLEAN_ROMAN = re.compile(r"\b([Ii][Xx]|[Ii][Vv]|[Vv]?[Ii]{0,3})\b")

# matches valid tags
_RE_VALID_TAGS = re.compile(r"^[\w_-]+$")

# split pages
_RE_PAGES_SPLIT = re.compile(r"[-]+")

# https://github.com/JelteF/PyLaTeX/blob/1a73261b771ae15afbb3ca5f06d4ba61328f1c62/pylatex/utils.py#L14-L30
LATEX_SPECIAL_CHARACTERS = {
    "&": r"\&",
    "%": r"\%",
    "$": r"\$",
    "#": r"\#",
    "_": r"\_",
    # NOTE: are used in title capitalization
    # "{": r"\{",
    # "}": r"\}",
    "~": r"\textasciitilde{}",
    "^": r"\^{}",
    "\\": r"\textbackslash{}",
    "\n": "\\newline%\n",
    # NOTE: shows up in pages
    # "-": r"{-}",
    # NOTE: non-breaking space
    "\xa0": "~",
    "[": r"{[}",
    "]": r"{]}",
}


def is_title_like(key: str) -> bool:
    return key in TITLE_LIKE_KEYS


def get_uppercase_words() -> dict[str, str]:
    return {
        w.upper(): w
        for w in _DEFAULT_UPPERCASE_WORDS | _get_user_words("uppercase-words")
    }


def get_capitalize_words() -> dict[str, str]:
    return {
        w.lower(): w
        for w in _DEFAULT_CAPITALIZE_WORDS | _get_user_words("capitalize-words")
    }


# {{{ reference


def ref_cleaner(
    doc: papis.document.Document, ref_format: FormattedString | str | None = None
) -> str | None:
    if ref_format is None:
        from papis_extras.utils import getformattedstring

        ref_format = getformattedstring("ref-format", section="extras-clean")

    from papis_extras.utils import make_custom_reference

    # try to get a reference
    ref = make_custom_reference(doc, ref_format) if ref_format else None

    # but leave it alone if it already exists in the document
    original_ref = doc.get("ref")
    if original_ref is not None and ref is not None and original_ref.startswith(ref):
        ref = None

    return ref


# }}}


# {{{ timestamp


def timestamp_cleaner(doc: papis.document.Document) -> str | None:
    if "time-added" in doc:
        return str(doc["time-added"])

    if "files" not in doc:
        return None

    def _getctime(f: str) -> float:
        folder = doc.get_main_folder()
        if folder is not None:
            f = os.path.join(folder, f)

        return os.path.getctime(f)

    from datetime import datetime

    time_added = min(_getctime(f) for f in doc["files"])
    return datetime.fromtimestamp(time_added).strftime(papis.strings.time_format)


# }}}


# {{{ author / editor


def author_list_cleaner(doc: papis.document.Document) -> tuple[dict[str, Any], ...]:
    def _clean_given(given: str) -> str:
        return string_cleaner_impl(
            _RE_CLEAN_DOTS.sub(r"\1. ", given), titlecased=True, capitalized=False
        )

    def _clean_family(family: str) -> str:
        # NOTE: don't titlecase here because we can have names
        # like "de la Fontaine", and it would be a hassle to catch that
        return string_cleaner_impl(family, titlecased=False, capitalized=False)

    lang = doc.get("lang", doc.get("language", "en")).lower()
    lang = lang if lang else "en"

    author_list = []
    for au in doc["author_list"]:
        author_list.append({
            "given": _clean_given(au.get("given", "")),
            "family": _clean_family(au.get("family", "")),
            "affiliation": iterable_cleaner_impl(au.get("affiliation", []), lang=lang),
        })

    return tuple(author_list)


# }}}


# {{{ files


def set_files_author(doc: papis.document.Document) -> str | None:
    if "author_list" not in doc:
        return None

    from papis_extras.utils import author_list_to_author

    prev_author = str(doc["author"])

    separator = papis.config.getstring("file-author-separator", section="extras-clean")
    if separator is None:
        separator = papis.config.getstring("multiple-authors-separator")

    author_format = getformattedstring("file-author-format", section="extras-clean")
    if author_format is None:
        author_format = getformattedstring("multiple-authors-format")

    if separator is None or author_format is None:
        return None

    doc["author"] = author_list_to_author(
        doc, max_authors=4, separator=separator, author_format=author_format
    )

    return prev_author


def files_cleaner(doc: papis.document.Document) -> tuple[str, ...] | None:
    if "files" not in doc:
        return None

    from string import ascii_lowercase

    from papis.commands.add import get_file_name

    g = papis.utils.create_identifier(ascii_lowercase)

    files = []
    suffix = ""

    # FIXME: there should be a nicer way to copy documents
    tmp = papis.document.from_data(doc.copy())
    folder = doc.get_main_folder()
    if folder is not None:
        tmp.set_folder(folder)

    # pylint: disable=E1136,E1137
    tmp["title"] = tmp["title"].replace("/", "-")
    set_files_author(tmp)

    has_new_files = False
    for old_file_path in tmp.get_files():
        new_filename = papis.utils.clean_document_name(
            get_file_name(tmp, old_file_path, suffix=suffix)
        )

        old_filename = os.path.basename(old_file_path)
        if old_filename != new_filename:
            has_new_files = True

        files.append(new_filename)
        suffix = next(g)

    return tuple(files) if has_new_files else None


# }}}


# {{{ folder


def folder_cleaner(doc: papis.document.Document) -> str | None:
    old_path = doc.get_main_folder()
    if old_path is None:
        return old_path

    folder_format = getformattedstring("add-folder-name")
    if not folder_format:
        return None

    # find new folder name
    from string import ascii_lowercase

    g = papis.utils.create_identifier(ascii_lowercase)
    suffix = ""

    def get_new_path(suffix: str) -> str:
        new_folder = papis.utils.clean_document_name(
            papis.format.format(folder_format, doc)
        )
        new_suffix = f"-{suffix}" if suffix else ""

        new_path = os.path.join(old_dirname, f"{new_folder}{new_suffix}")

        return new_path

    prev_author = set_files_author(doc)
    old_dirname = os.path.dirname(old_path)
    rename = False

    while True:
        new_path = get_new_path(suffix)
        suffix = next(g)

        # nothing to do if path didn't change
        if new_path == old_path:
            break

        # just rename if new path does not exist
        if not os.path.exists(new_path):
            rename = True
            break

        # check if new path is a duplicate
        new_doc = papis.document.from_folder(new_path)
        result = papis.utils.locate_document(new_doc, [doc])
        if result:
            logger.warning(
                "{c.Fore.YELLOW}%s{c.Style.RESET_ALL}: duplicate found.", new_path
            )
            break

    if prev_author is not None:
        doc["author"] = prev_author

    return new_path if rename else None


# }}}


# {{{ tags


def is_valid_tag(tag: str) -> bool:
    return _RE_VALID_TAGS.match(tag) is not None


def tags_cleaner(doc: papis.document.Document) -> str | None:
    if "tags" not in doc:
        return None

    tags = doc["tags"]
    if isinstance(tags, list):
        pass
    elif isinstance(tags, str):
        tags = [tag.strip(",") for tag in tags.split(" ")]
    else:
        raise TypeError(f"Unsupported tag type: '{type(tags).__name__}'.")

    for tag in tags:
        if not is_valid_tag(tag):
            logger.warning("'%s' is not a valid tag.", tag)

    return " ".join(tags)


# }}}


# {{{ pages


def _parse_pages(doc: papis.document.Document) -> tuple[str, str] | None:
    def _integerify(value: str) -> str:
        try:
            return str(int(value))
        except ValueError:
            return value

    value = doc["pages"]
    pages: tuple[str, ...] = ()
    if isinstance(value, (str, int)):
        pages = tuple(_integerify(p) for p in _RE_PAGES_SPLIT.split(str(value)))
    elif isinstance(value, list):
        pages = tuple(str(p) for p in value)
    else:
        raise TypeError(f"Unsupported 'pages' type: '{type(value).__name__}'.")

    if not "".join(pages):
        return None

    if len(pages) == 1:
        return (pages[0], pages[0])
    elif len(pages) == 2:
        return (pages[0], pages[1])
    else:
        raise ValueError(f"Unknown pages format: '{value}'.")


def pages_cleaner(doc: papis.document.Document) -> str | None:
    try:
        pages = _parse_pages(doc)
    except ValueError:
        pages = None

    if pages is None:
        return pages

    return f"{pages[0]}-{pages[1]}"


def pages_bibtex_cleaner(doc: papis.document.Document) -> str | None:
    pages = _parse_pages(doc)
    if pages is None:
        return pages

    return f"{pages[0]}--{pages[1]}"


# }}}


# {{{ month cleaner

MONTH_NAMES = (
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
)

SHORT_MONTH_NAMES = tuple([m[:3] for m in MONTH_NAMES])  # noqa: C409


def month_cleaner(value: Any) -> int | None:
    month = None
    if isinstance(value, int):
        month = value
    elif isinstance(value, str):
        value = value.lower()
        try:
            month = MONTH_NAMES.index(value) + 1
        except ValueError:
            try:
                month = SHORT_MONTH_NAMES.index(value) + 1
            except ValueError:
                with suppress(ValueError):
                    month = int(value)

    return month


# }}}


# {{{ strings


def string_cleaner_impl(
    value: str,
    *,
    lang: str = "en",
    titlecased: bool = False,
    capitalized: bool = True,
    bibtex: bool = False,
) -> str:
    uppercase_words = get_uppercase_words()
    capitalize_words = get_capitalize_words()

    def capitalize(
        word: str, capitalizations: dict[str, str], *, exact: bool = False
    ) -> str | None:
        # capitalize word according to given rules
        word_stripped = word.strip("().")
        if word_stripped in capitalizations:
            word_cap = word.replace(word_stripped, capitalizations[word_stripped])
            return f"{{{word_cap}}}" if bibtex else word_cap

        # capitalize more complex setups like `Navier-Stokes`
        words_cap = sorted([w for w in capitalizations if w in word])
        if words_cap:
            word_split = [w.strip("().") for w in _RE_CLEAN_SPLIT.split(word)]

            capitalized = False
            word_cap = word
            for i, ws in enumerate(word_split):
                found = False
                for w in words_cap:
                    if (exact and ws == w) or (not exact and ws.startswith(w)):
                        word_cap = word_cap.replace(w, capitalizations[w], 1)
                        found = True
                        capitalized = True
                        break

                if not found and (len(ws) > 1 or i == 0):
                    if lang in {"en", "de"}:
                        word_cap = word_cap.replace(ws, ws.capitalize(), 1)
                    else:
                        word_cap = word_cap.replace(ws, ws.lower(), 1)

            if not capitalized:
                return None

            return f"{{{word_cap}}}" if (bibtex and capitalized) else word_cap

        return None

    def titlecase_handler(word: str, **kwargs: Any) -> str | None:
        if not word:
            return word

        # keep math in titles as is
        if "$" in word or "\\" in word:
            return word

        # remove any other curly braces
        if "{" in word and "}" in word:
            return word if bibtex else word.replace("{", "").replace("}", "")

        word_upper = word.upper()
        word_lower = word.lower()

        if capitalized:
            # uppercase word
            word_cap = capitalize(word_upper, uppercase_words, exact=True)
            if word_cap is not None:
                return word_cap

            # capitalize word
            word_cap = capitalize(word_lower, capitalize_words)
            if word_cap is not None:
                return word_cap

        if titlecased:
            m = _RE_CLEAN_ROMAN.match(word)
            if m and m.group():
                # NOTE: this returns the uppercased word so that `titlecase`
                # can re-capitalize it from scratch, otherwise it would be
                # confused by cases like McDONALD
                return word_upper

        if lang in {"en", "de"}:
            return None if titlecased or word[0].isupper() else word
        else:
            return word_lower

    value = value.replace("\n", " ")
    value = _RE_CLEAN_HTML.sub("", value)
    value = _RE_CLEAN_WHITESPACE.sub(" ", value).strip()
    if titlecased:
        # NOTE: we do not want to do this in a normal string because it could
        # replace "i.e." by "i. E." and other valid uses.
        value = _RE_CLEAN_FULLSTOP.sub(lambda p: f". {p.group(1).upper()}", value)

    import titlecase

    value = titlecase.titlecase(value, callback=titlecase_handler)

    if lang not in {"en", "de"}:
        value = f"{value[0].upper()}{value[1:]}"

    return value


def string_cleaner(
    doc: papis.document.Document,
    key: str,
    *,
    titlecased: bool = False,
    bibtex: bool = False,
) -> str:
    lang = doc.get("lang", doc.get("language", "en")).lower()
    lang = lang if lang else "en"

    if key in {"journal", "publisher"}:
        lang = "en"

    return string_cleaner_impl(
        doc[key], lang=lang, titlecased=titlecased, bibtex=bibtex
    )


# }}}


# {{{ iterable


def iterable_cleaner_impl(
    value: Any, *, lang: str = "en", titlecased: bool = False, bibtex: bool = False
) -> Any:
    if len(value) == 0:
        return value

    if isinstance(value, (list, tuple)):
        return type(value)([
            iterable_cleaner_impl(f, lang=lang, titlecased=titlecased, bibtex=bibtex)
            for f in value
        ])
    elif isinstance(value, dict):
        return {
            k: iterable_cleaner_impl(v, lang=lang, titlecased=titlecased, bibtex=bibtex)
            for k, v in value.items()
        }
    elif isinstance(value, str):
        return string_cleaner_impl(
            value, lang=lang, titlecased=titlecased, bibtex=bibtex
        )
    elif isinstance(value, (int, float)):
        return value
    else:
        raise TypeError(f"Unsupported type: '{type(value).__name__}'.")


def iterable_cleaner(
    doc: papis.document.Document,
    key: str,
    *,
    titlecased: bool = False,
    bibtex: bool = False,
) -> Any:
    lang = doc.get("lang", doc.get("language", "en")).lower()
    return iterable_cleaner_impl(
        doc[key], lang=lang, titlecased=titlecased, bibtex=bibtex
    )


# }}}


# {{{ escape_latex


def escape_latex(s: T) -> T:
    if isinstance(s, str):
        return "".join(LATEX_SPECIAL_CHARACTERS.get(c, c) for c in s)  # type: ignore
    else:
        return s


# }}}
