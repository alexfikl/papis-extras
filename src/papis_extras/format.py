# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import string
from typing import Any, ClassVar

import papis.logging
from papis.document import Document, from_data
from papis.format import Jinja2Formatter as Jinja2FormatterBase
from papis.format import PythonFormatter as PythonFormatterBase
from papis.format import unescape
from papis_extras.utils import make_custom_reference

logger = papis.logging.get_logger(__name__)


class FormatFailedError(Exception):
    pass


# {{{ pythonext


class PythonExtFormatter(PythonFormatterBase):
    name: ClassVar[str] = "python_ext"

    def format(
        self,
        fmt: str,
        doc: Document | dict[str, Any],
        doc_key: str = "",
        additional: dict[str, Any] | None = None,
        default: str | None = None,
    ) -> str:
        if additional is None:
            additional = {}

        fmt = unescape(fmt)
        if not isinstance(doc, Document):
            doc = from_data(doc)
        doc_name = doc_key or self.default_doc_name

        psf = getattr(self, "psf", string.Formatter())
        try:
            if fmt == "extras-ref-format":
                fmt = make_custom_reference(doc) or "#REF"

            return psf.format(fmt, **{doc_name: doc}, **additional)
        except Exception as exc:
            if default is not None:
                logger.warning(
                    "Could not format string '%s' for document '%s'",
                    fmt,
                    papis.document.describe(doc),
                    exc_info=exc,
                )
                return default
            else:
                raise FormatFailedError(fmt) from exc


# }}}


# {{{ jinja2ext


def jinja2_and_others(authors: list[Any], maximum: int = 4) -> str:
    family = [a["family"] for a in authors[:maximum]]
    if len(authors) > maximum:
        family += ["and", "others"]

    return "-".join(family)


class Jinja2ExtFormatter(Jinja2FormatterBase):
    name: ClassVar[str] = "jinja2_ext"
    env: ClassVar[Any] = None

    @classmethod
    def get_environment(cls, *, force: bool = False) -> Any:
        if cls.env is None or force:
            from jinja2 import Environment

            # NOTE: this will kindly autoescape apostrophes otherwise
            env = Environment(autoescape=False)  # noqa: S701
            env.shared = True
            env.filters["and_others"] = jinja2_and_others

            cls.env = env

        return cls.env

    def format(
        self,
        fmt: str,
        doc: Document | dict[str, Any],
        doc_key: str = "",
        additional: dict[str, Any] | None = None,
        default: str | None = None,
    ) -> str:
        if additional is None:
            additional = {}

        fmt = unescape(fmt)
        if not isinstance(doc, papis.document.Document):
            doc = papis.document.from_data(doc)

        doc_name = doc_key or self.default_doc_name
        env = self.get_environment()

        try:
            return str(env.from_string(fmt).render(**{doc_name: doc}, **additional))
        except Exception as exc:
            if default is not None:
                logger.warning(
                    "Could not format string '%s' for document '%s'",
                    fmt,
                    papis.document.describe(doc),
                    exc_info=exc,
                )
                return default
            else:
                raise FormatFailedError(fmt) from exc


# }}}
