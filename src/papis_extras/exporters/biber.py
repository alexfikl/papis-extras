# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
A an alternative BibTeX exporter to the one with ``papis export --format bibtex``.
The main changes are:

* It updates keys (but does not save them back) using the latest settings in the
  configuration. This is not very exciting if the ``papis-extras-clean`` command
  is used regularly, but could be useful.
* The reference uses the format from ``papis-extras-clean`` as well.
* Will probably be tweaked more.

Examples
^^^^^^^^

Can be used with the default :mod:`~papis.commands.export` command as

.. code::

    papis export Einstein --format biber

Options
^^^^^^^

.. papis-config:: indent
    :section: extras-exporter-biber

    Number of spaces to indent with. Some people (present company included)
    are very peculiar about these things.
"""

from __future__ import annotations

import papis.config
import papis.document
import papis.logging
import papis_extras.config  # noqa: F401

logger = papis.logging.get_logger(__name__)


def get_doc_type(doc: papis.document.Document) -> str:
    from papis.bibtex import bibtex_type_converter, bibtex_types

    bibtype = "article"
    if "type" in doc:
        if doc["type"] in bibtex_types:
            bibtype = doc["type"]
        elif doc["type"] in bibtex_type_converter:
            bibtype = doc[bibtex_type_converter[doc["type"]]]

    return bibtype


def export_doc_biblatex(doc: papis.document.Document, indent: int = 4) -> str:
    from papis.bibtex import bibtex_key_converter, bibtex_keys
    from papis_extras.utils import author_list_to_author

    ignore_keys = papis.config.getlist("bibtex-ignore-keys")
    if ignore_keys is None:
        ignore_keys = ["language", "month"]
    ignore_keys += ["type"]

    import papis_extras.cleaners as cl

    lines = []
    for key in sorted(doc):
        key_name = bibtex_key_converter.get(key, key)
        if key_name not in bibtex_keys:
            continue

        if key_name in ignore_keys:
            continue

        value = doc[key]
        if key == "author":
            value = cl.escape_latex(author_list_to_author(doc, max_authors=5))
        elif key == "pages":
            value = cl.pages_bibtex_cleaner(doc)
        elif key == "doi":
            value = value.strip()
            if not value.startswith("http"):
                value = f"https://doi.org/{value}"
        elif key == "file":
            value = "; ".join(doc["files"])
        elif key == "url":
            if "doi" in doc:
                value = None
        elif isinstance(value, str):
            value = cl.string_cleaner(
                doc, key, titlecased=cl.is_title_like(key), bibtex=True
            )
            if cl.is_title_like(key):
                value = cl.escape_latex(value)

        if value is not None:
            lines.append(f"{key_name} = {{ {value} }}")

    from papis_extras.utils import get_doc_ref

    bibtype = get_doc_type(doc)
    ref = get_doc_ref(doc)
    spaces = " " * indent

    fields = f",\n{spaces}".join(lines)
    return f"@{bibtype}{{{ref},\n{spaces}{fields}\n}}"


def exporter(documents: list[papis.document.Document]) -> str:
    """Convert documents into a list of **modern** BibLaTeX entries"""
    indent = papis.config.getint("indent", section="extras-exporter-biber")
    if indent is None:
        indent = 4
    assert indent >= 0

    bibtex = "\n\n".join(export_doc_biblatex(doc, indent=indent) for doc in documents)

    clipboard = papis.config.getboolean("clipboard", section="extras-exporter")
    if clipboard is None:
        clipboard = False

    if clipboard:
        from papis_extras.utils import copy_to_clipboard

        copy_to_clipboard(bibtex)

    return bibtex
