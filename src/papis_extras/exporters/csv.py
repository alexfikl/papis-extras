# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
A simple CSV exporter used to export the document keys.

Examples
^^^^^^^^

Can be used with the default :mod:`~papis.commands.export` command as

.. code::

    papis export Einstein --format csv

Options
^^^^^^^

.. papis-config:: delimiter
    :section: extras-exporter-csv

    A delimiter to use in the resulting CSV file.

.. papis-config:: dialect
    :section: extras-exporter-csv

    The dialect of the exporter CSV, as detailed in :mod:`csv`.

.. papis-config:: keys
    :section: extras-exporter-csv

    A list of document keys that should be exported. Entries for keys that
    do not exist in the document are left empty. This also supports a few special
    entries:

    * ``"index"``: adds an index column to the resulting table.
    * ``"<key>_count"``: adds a column with the number of elements in the
      suffixed key. For example, this can be used to append a column with the
      number of artists by adding ``"author_list_count"``.
"""

from __future__ import annotations

import papis.config
import papis.document
import papis.logging
import papis_extras.config  # noqa: F401

logger = papis.logging.get_logger(__name__)


def get_volume_issue_pages(doc: papis.document.Document) -> str:
    result = []

    if "volume" in doc:
        result.append(f"Vol. {doc['volume']}")

    if "issue" in doc:
        result.append(f"nr. {doc['issue']}")
    elif "number" in doc:
        result.append(f"nr. {doc['number']}")

    if "pages" in doc:
        import papis_extras.cleaners as cl

        pages = cl.pages_bibtex_cleaner(doc)
        result.append(f"pp. {pages}".replace("--", "-"))

    return ", ".join(result)


def select_document_keys(
    index: int, doc: papis.document.Document, keys: list[str]
) -> dict[str, int | str | None]:
    from papis_extras.utils import author_list_to_author

    entries: dict[str, int | str | None] = {}
    for key in keys:
        if key == "index":
            entries[key] = index + 1
        elif key == "volume_issue_page":
            entries[key] = get_volume_issue_pages(doc)
        elif key in {"author", "author_list"}:
            entries[key] = author_list_to_author(doc)
        elif key.endswith("_count"):
            doc_key = key[:-6]
            if doc_key in doc:
                value = doc[doc_key]
                if isinstance(value, list):
                    entries[key] = len(value)
                else:
                    logger.error("Key '%s' is not a list: '%s'.", doc_key, value)
        elif key in doc:
            entries[key] = doc[key]
        else:
            entries[key] = None

    return entries


def exporter(documents: list[papis.document.Document]) -> str:
    """Convert documents to the CSV format"""
    delimiter = papis.config.get("delimiter", section="extras-exporter-csv")
    dialect = papis.config.get("dialect", section="extras-exporter-csv")
    keys = papis.config.getlist("keys", section="extras-exporter-csv")

    if delimiter is None:
        delimiter = ","
    delimiter = delimiter.strip()

    if dialect is None:
        dialect = "excel"
    dialect = dialect.lower()

    import csv
    import io

    dialects = csv.list_dialects()
    if dialect not in dialects:
        logger.error(
            "Unknown CSV dialect '%s'. Available dialects are '%s'.",
            dialect,
            "', '".join(dialects),
        )

    buffer = io.StringIO()
    writer = csv.DictWriter(
        buffer,
        fieldnames=keys,
        dialect=dialect,
        delimiter=delimiter,
        quoting=csv.QUOTE_NONNUMERIC,
    )
    writer.writeheader()

    for i, doc in enumerate(documents):
        writer.writerow(select_document_keys(i, doc, keys))

    result = buffer.getvalue()

    clipboard = papis.config.getboolean("clipboard", section="extras-exporter")
    if clipboard is None:
        clipboard = False

    if clipboard:
        from papis_extras.utils import copy_to_clipboard

        copy_to_clipboard(result)

    return result
