# SPDX-FileCopyrightText: 2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
An exporter for ``reStructuredText``, that will basically just print to `stdout`.

.. code::

    .. [REF] Author, Author, Author,
        *Title*,
        Journal/Publisher, Vol. N, pages XX-XX, year,
        `URL <url>`__.

Examples
^^^^^^^^

Can be used with the default :mod:`~papis.commands.export` command as

.. code::

    papis export Einstein --format sphinx
"""

from __future__ import annotations

import papis.config
import papis.document
import papis.format
import papis.logging
import papis_extras.config  # noqa: F401

logger = papis.logging.get_logger(__name__)


def export_doc_sphinx(
    doc: papis.document.Document,
    indent_width: int | None = None,
) -> str:
    from papis_extras.utils import author_list_to_author

    if indent_width is None:
        indent_width = 4
    indent = " " * indent_width

    import papis_extras.cleaners as cl

    def get(key: str) -> str:
        return cl.string_cleaner(doc, key, titlecased=cl.is_title_like(key))

    from papis_extras.utils import get_doc_ref

    ref = get_doc_ref(doc)
    authors = author_list_to_author(doc, separator=", ", max_authors=5)

    lines = []
    lines.append(f".. [{ref}] {authors}")
    lines.append(f"*{get('title')}*")

    publication = []
    if "journal" in doc:
        publication.append(get("journal"))
    elif "publisher" in doc:
        publication.append(get("publisher"))
    elif "school" in doc:
        publication.append(get("school"))
    elif "archiveprefix" in doc:
        publication.append(doc["archiveprefix"])

    if "volume" in doc:
        publication.append(f"Vol. {doc['volume']}")
    if "pages" in doc:
        pages = cl.pages_bibtex_cleaner(doc)
        if pages is not None:
            publication.append(f"pp. {pages}")
    if "year" in doc:
        publication.append(doc["year"])
    lines.append(", ".join(str(p) for p in publication))

    if "doi" in doc:
        url_name = "DOI"
        url = doc["doi"]
        if not url.startswith("http"):
            url = f"https://doi.org/{url}"
    elif "url" in doc:
        url_name = "URL"
        url = doc["url"]
    else:
        url = url_name = None

    if url is not None:
        assert url_name is not None

        # TODO: better to quote the full url?
        url = url.replace("<", "%3C").replace(">", "%3E")

        lines.append(f"`{url_name} <{url}>`__")

    result = f",\n{indent}".join(lines) + "."

    return result


def exporter(documents: list[papis.document.Document]) -> str:
    """Convert documents to a list of Sphinx reference entries"""
    plain = "\n\n".join(export_doc_sphinx(doc) for doc in documents)

    clipboard = papis.config.getboolean("clipboard", section="extras-exporter")
    if clipboard is None:
        clipboard = False

    if clipboard:
        from papis_extras.utils import copy_to_clipboard

        copy_to_clipboard(plain)

    return plain
