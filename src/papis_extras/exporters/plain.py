# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

"""
A very simple exporter, that will basically just print to `stdout`.

.. code::

    Author, Author, Author,
    Title,
    Journal/Publisher, Vol. N, pages XX--XX, year,
    URL

Examples
^^^^^^^^

Can be used with the default :mod:`~papis.commands.export` command as

.. code::

    papis export Einstein --format plain

Options
^^^^^^^

.. papis-config:: bibitem
    :section: extras-exporter-plain

    If true, it makes the output more LaTeX friendly for appending as
    an inline ``bibitem`` with the document reference.
"""

from __future__ import annotations

import papis.config
import papis.document
import papis.format
import papis.logging
import papis_extras.config  # noqa: F401

logger = papis.logging.get_logger(__name__)


def export_doc_plain(
    doc: papis.document.Document,
    *,
    use_bibitem: bool = True,
    indent_width: int | None = None,
) -> str:
    from papis_extras.utils import author_list_to_author

    if indent_width is None:
        indent_width = 4 if use_bibitem else 0
    indent = " " * indent_width

    # {{{ bibitem formatting

    from papis_extras.utils import get_doc_ref

    ref = get_doc_ref(doc)

    header = None
    if use_bibitem:
        header = r"\bibitem{%s}" % ref  # noqa: UP031

    title_format = r"\textit{%s}" if use_bibitem else "%s"
    url_format = r"\url{%s}" if use_bibitem else "%s"

    # }}}

    import papis_extras.cleaners as cl

    def get(key: str) -> str:
        return cl.escape_latex(
            cl.string_cleaner(doc, key, titlecased=cl.is_title_like(key), bibtex=True)
        )

    lines = []
    lines.append(author_list_to_author(doc, separator=", ", max_authors=5))
    lines.append(title_format % get("title"))

    publication = []
    if "journal" in doc:
        publication.append(get("journal"))
    elif "publisher" in doc:
        publication.append(get("publisher"))
    elif "school" in doc:
        publication.append(get("school"))
    elif "archiveprefix" in doc:
        publication.append(doc["archiveprefix"])

    if "volume" in doc:
        publication.append(f"Vol. {doc['volume']}")
    if "pages" in doc:
        pages = cl.pages_bibtex_cleaner(doc)
        if pages is not None:
            publication.append(f"pp. {pages}")
    if "year" in doc:
        publication.append(doc["year"])
    lines.append(", ".join(str(p) for p in publication))

    if "doi" in doc:
        doi = doc["doi"]
        if not doi.startswith("http"):
            doi = f"https://doi.org/{doi}"

        lines.append(url_format % doi)
    elif "url" in doc:
        lines.append(url_format % doc["url"])

    result = f",\n{indent}".join(lines) + "."
    if header is not None:
        result = f"{header}\n{indent}{result}"

    return result


def exporter(documents: list[papis.document.Document]) -> str:
    """Convert documents to a list of ``bibitem`` entries"""
    use_bibitem = papis.config.getboolean("bibitem", section="extras-exporter-plain")
    if use_bibitem is None:
        use_bibitem = True

    plain = "\n\n".join(
        export_doc_plain(doc, use_bibitem=use_bibitem) for doc in documents
    )

    clipboard = papis.config.getboolean("clipboard", section="extras-exporter")
    if clipboard is None:
        clipboard = False

    if clipboard:
        from papis_extras.utils import copy_to_clipboard

        copy_to_clipboard(plain)

    return plain
