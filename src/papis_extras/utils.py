# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

from typing import Any, TypeVar

import papis.config
import papis.document
import papis.logging

try:
    from papis.strings import (  # type: ignore[attr-defined,unused-ignore]
        FormattedString,
    )
except ImportError:
    FormattedString = str  # type: ignore[assignment,misc,unused-ignore]

logger = papis.logging.get_logger(__name__)

T = TypeVar("T")


def copy_to_clipboard(data: str) -> None:
    try:
        import pyperclip

        pyperclip.copy(data)
        logger.info("Copied to system clipboard.")
    except ImportError as exc:
        logger.error("Cannot copy to system clipboard.", exc_info=exc)


def abbrev_author_given(author: dict[str, str]) -> dict[str, str]:
    def _split(name: str, separators: list[str]) -> str:
        if not name:
            return name

        if not separators:
            return f"{name[0]}."

        sep = separators.pop(0)
        return sep.join(_split(n, separators) for n in name.split(sep))

    return {
        "given": _split(author.get("given", ""), [" ", "-"]),
        "family": author.get("family", ""),
    }


def author_list_to_author(
    doc: papis.document.Document,
    *,
    separator: str | None = None,
    author_format: FormattedString | None = None,
    max_authors: int | None = None,
    abbrev_given: bool = True,
) -> str:
    if separator is None:
        separator = papis.config.get("multiple-authors-separator")
        if separator is None:
            separator = ", "

    if " " not in separator:
        separator = f" {separator.strip()} "

    if author_format is None:
        author_format = papis.config.get("multiple-authors-format")
        if author_format is None:
            author_format = "{au[given]} {au[family]}"  # type: ignore[assignment,unused-ignore]

    if max_authors is None:
        max_authors = 128

    if abbrev_given:
        abbrev = abbrev_author_given
    else:

        def abbrev(author: dict[str, str]) -> dict[str, str]:
            return author

    if "author_list" not in doc:
        return str(doc["author"]).strip()

    author = separator.join(
        papis.format.format(
            author_format,  # type: ignore[arg-type,unused-ignore]
            abbrev(author),
            doc_key="au",
        )
        for author in doc["author_list"][:max_authors]
    )

    if len(doc["author_list"]) > max_authors:
        author = f"{author} and others"

    return author.strip()


def split_author_name(author: str) -> dict[str, str]:
    """Splits an author name into a family and given name."""

    from bibtexparser.customization import splitname

    parts = splitname(author)
    given = " ".join(parts["first"])
    family = " ".join(parts["von"] + parts["last"] + parts["jr"])

    return {"family": family, "given": given}


def sort_by_reference(ary: list[T], refs: list[str]) -> list[T]:
    idx = [i[0] for i in sorted(enumerate(refs), key=lambda x: x[1])]
    return [ary[i] for i in idx]


def get_doc_ref(doc: papis.document.Document) -> str:
    if "ref" in doc:
        return str(doc["ref"])

    from papis_extras.cleaners import ref_cleaner

    ref_format = getformattedstring("ref-format", section="extras-clean")
    ref = ref_cleaner(doc, ref_format)
    if ref is None:
        ref = "unknown"

    return ref


def getformattedstring(key: str, section: str | None = None) -> FormattedString:
    try:
        return papis.config.getformattedstring(key, section=section)  # type: ignore[attr-defined,unused-ignore]
    except AttributeError:
        return papis.config.getstring(key, section=section)  # type: ignore[return-value,unused-ignore]


def make_custom_reference(
    doc: papis.document.Document, ref_format: FormattedString | str | None = None
) -> str | None:
    from slugify import slugify

    from papis_extras.cleaners import _RE_CLEAN_SPLIT

    def make_clean_name(name: str) -> str:
        names = _RE_CLEAN_SPLIT.split(slugify(name))
        return "".join(x.capitalize() for x in names)

    def make_clean_names(author_list: tuple[dict[str, Any], ...]) -> tuple[str, ...]:
        return tuple(make_clean_name(au.get("family", "")) for au in author_list)

    if ref_format is None:
        ref_format = getformattedstring("ref-format", section="extras-clean")

    ref = None
    ref_format_value = str(ref_format)
    if ref_format_value == "doi":
        ref = str(doc["doi"]) if "doi" in doc else None
    elif ref_format_value == "isbn":
        ref = doc.get("isbn", doc.get("eisbn", doc.get("isbn-13", None)))
    elif ref_format_value == "first-author-year":
        if "author_list" in doc:
            author = make_clean_name(doc["author_list"][0].get("family", ""))
        else:
            author = ""

        if author:
            ref = f"{author}{doc['year']}"
    elif ref_format_value == "initials-year":
        initials = "".join(
            f[0] for f in make_clean_names(doc.get("author_list", [])) if f
        )

        if initials:
            ref = f"{initials}{doc['year']}"
    elif ref_format_value == "authors-year":
        names = "".join(make_clean_names(doc["author_list"][:4]))
        if names:
            ref = f"{names}{doc['year']}"
    else:
        ref = papis.format.format(ref_format, doc)

    if ref:
        ref = ref.replace(" ", "")

    return ref
