# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import papis.config

# {{{ commands

# clean
papis.config.register_default_settings({
    "extras-clean": {
        "keys": ["author", "title", "time-added", "files", "folder"],
        "ref-format": "first-author-year",
        "file-author-format": "{au[family]}",
        "file-author-separator": "-",
    }
})

# new
papis.config.register_default_settings({
    "extras-new": {
        "link": False,
        "move": False,
    }
})

# }}}

# {{{ exporters

# general
papis.config.register_default_settings({
    "extras-exporter": {
        "clipboard": True,
        "uppercase-words": [],
        "capitalize-words": [],
    }
})

# biber
papis.config.register_default_settings({
    "extras-exporter-biber": {
        "indent": 4,
    }
})

# plain
papis.config.register_default_settings({
    "extras-exporter-plain": {
        "bibitem": True,
    }
})

# csv
papis.config.register_default_settings({
    "extras-exporter-csv": {
        "delimiter": ",",
        "dialect": "excel",
        "keys": [
            "index",
            "title",
            "author_list",
            "journal",
            "volume_issue_page",
            "year",
            "author_list_count",
        ],
    }
})

# }}}
