# SPDX-FileCopyrightText: 2020-2023 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

from __future__ import annotations

import io
import re
import unicodedata

import papis.config
import papis.logging

logger = papis.logging.get_logger(__name__)

# LTWA
LTWA_URL = "https://www.issn.org/services/online-services/access-to-the-ltwa/"
LTWA_DEFAULT_CSV_URL = (
    "https://www.issn.org/wp-content/uploads/2021/07/ltwa_20210702.csv"
)

# tokenization
_RE_WORD_TOKENIZER = re.compile(r"[\s-]+")
_RE_LANGUAGE_TOKENIZER = re.compile(r"\s*,\s*")


def get_latest_ltwa_url() -> str:
    import bs4
    import requests

    response = requests.get(
        LTWA_URL,
        headers={"User-Agent": papis.config.getstring("user-agent")},
        timeout=5,
    )
    soup = bs4.BeautifulSoup(response.content, features="lxml")
    results = soup.select("div.entry > p > a")

    for a in results:
        if "csv" in a["href"]:
            return str(a["href"])

    logger.warning(
        "Could not find an up to date URL. Using default '%s'", LTWA_DEFAULT_CSV_URL
    )

    return LTWA_DEFAULT_CSV_URL


def parse_ltwa(file: io.StringIO) -> dict[str, dict[str, str]]:
    import csv

    reader = csv.reader(file, delimiter=";")

    # NOTE: skip the header
    _ = next(reader)

    ltwa: dict[str, dict[str, str]] = {}
    for row in reader:
        word, abbrev, langs = row
        for lang in _RE_LANGUAGE_TOKENIZER.split(langs):
            d = ltwa.setdefault(lang, {})
            d[word] = abbrev

    return ltwa


def abbreviate(title: str, abbrevs: dict[str, str], *, periods: bool = True) -> str:
    """
    :arg title: title to be abbreviated.
    :arg abbrevs: a dictionary of words and their abbreviations, as obtained from
        `LTWA <https://www.issn.org/services/online-services/access-to-the-ltwa/>`__.
    :arg periods: if *False*, do not include periods in the abbreviated result.
    """
    title = unicodedata.normalize("NFKD", title)
    return title
