papis-extras
============

Additional (personal) plugins of various types for [papis](https://github.com/papis/papis).

* `papis-extras-new`: command that adds the new document and also performs the
  sanitization done by the `clean` command below.
* `papis-extras-clean`: command that cleans / sanitizes a given list of keys
  in the queried documents.
* `papis-extras-exporter-biber`: an opinionated BibLaTeX exporter.
* `papis-extras-exporter-csv`: a CSV exporter configurable fields.
* `papis-extras-exporter-plain`: an additional simple exporter, to be used with
  inline bibliography (in the `thebibliography` environment).
* `papis-extras-exporter-sphinx`: exports document in the `reStructuredText`
  citation format used by the Sphinx documentation.

See the [documentation](https://papis-extras.readthedocs.io/en/latest/) for
more information about what and how they work.

License
=======

The code is licensed under the GPL3 license (see `LICENSES/GPL-3.0-or-later.txt`),
due to some code copied from papis, but most parts are under the MIT license
(see `LICENSES/MIT.txt`).
