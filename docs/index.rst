.. papis-extras documentation master file, created by
   sphinx-quickstart on Tue Apr  7 20:40:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to papis-extras's documentation!
========================================

Additional (personal) plugins of various types for
`papis <https://github.com/papis/papis>`_.

.. toctree::
   :maxdepth: 2

   commands
   exporters

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
