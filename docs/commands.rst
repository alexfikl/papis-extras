Commands
========

Clean
-----

.. automodule:: papis_extras.commands.clean

New
---

.. automodule:: papis_extras.commands.new
