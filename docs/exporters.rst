Exporters
=========

Common Options
--------------

.. papis-config:: clipboard
    :section: extras-exporter

    If *True*, we use `pyperclip <https://pypi.org/project/pyperclip/>`__ to
    also paste the output to the clipboard for easy manipulation.

.. papis-config:: uppercase-words
    :section: extras-exporter

    A path to a text file containing words that should be uppercased, one per
    line. If the main usecase is to export to BibLaTex, where the word will be
    uppercased and surrounded by curly braces, e.g ``Siam`` will be transformed
    to ``{SIAM}`` in the fields of the document.

.. papis-config:: capitalize-words
    :section: extras-exporter

    A path to a text file containing words that should maintain a fixed
    capitalization. This is similar to ``uppercase-words`` above, but it will
    transform ``Navier-stokes`` to the standard ``{Navier-Stokes}``.

Biber
-----

.. automodule:: papis_extras.exporters.biber

Plain
-----

.. automodule:: papis_extras.exporters.plain

CSV
---

.. automodule:: papis_extras.exporters.csv

Sphinx
------

.. automodule:: papis_extras.exporters.sphinx
